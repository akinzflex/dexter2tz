
# Dexter v2

## Introduction

Dexter is a decentralized non-custodial exchange for XTZ and [FA1.2](https://gitlab.com/tzip/tzip/blob/master/proposals/tzip-7/tzip-7.md) or [FA2](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md) tokens on the Tezos blockchain, based on [Uniswap v1](https://hackmd.io/@HaydenAdams/HJ9jLsfTz?type=view). Each Dexter smart contract is associated with a unique XTZ / token pair.

Any owner of XTZ and tokens can become a liquidity provider by depositing an equal value of XTZ and tokens into the smart contract. Similarly, any owner of XTZ or tokens may trade one asset for the other.

Liquidity providers are incentivized by receiving a proportional share of a 0.3% liquidity fee for each trade, paid by the trader. Liquidity providers may withdraw their provided XTZ and tokens at any time.

For contract specification and usage information, see the [informal specification](docs/informal-spec/dexter2.md) as well as [How to use Dexter via the command line](docs/dexter-cli.md) and [How to integrate a Dexter exchange into your application](docs/dexter-integration.md).

## Changes from Dexter v1

This second version is a ground-up rewrite in the Ligo language with the following major features:
- Fixes a fatal security flaw
- Adds a liquidity token
- Adds support for FA2 tokens

## Trading

Anyone can perform trades if they have enough XTZ or tokens to purchase a minimal amount according to the exchange rate.

If you hold XTZ and you want to purchase tokens, you can sell XTZ to Dexter and it will give you the equivalent amount of tokens minus 0.3% of your XTZ which is given to the liquidity providers as a fee.

The opposite relation holds true if you hold tokens and want to sell them to Dexter. It will give you an equivalent amount of XTZ, minus 0.3% of your tokens, which it give to the liquidity providers as a fee.

Finally, you can trade a token A for another token B. This works by trading A to XTZ then XTZ to B using an additional Dexter contract and ensures that you get your preferred exchange rate. There is a 0.3% fee paid to the XTZ/A contract and then another 0.3% fee paid to the XTZ/B contract.

## Liquidity Pools

Each Dexter contract holds two asset pools: one for XTZ and one for the token. Ownership of these liquidity pools is represented by liquidity tokens. When you add liquidity to a Dexter contract, it gives you liquidity tokens which represent your contribution to the pool. Liquidity tokens can be redeemed for an equivalent value of XTZ and tokens.

The number of liquidity tokens you own can only be changed through adding and removing liquidity. When other users trade one token for another, it does not affect the total amount of liquidity tokens you own. However, it does change the amounts of the liquidity pools.

When a trader swaps XTZ for tokens, it increases the the XTZ pool and decreases the token pool. This causes redeeming liquidity tokens to return more XTZ and less tokens. It also causes the XTZ to token exchange rate to decrease.

Conversely, when a trader swaps tokens for XTZ, it increases the token liquidity pool and decreases the XTZ pool. This causes redeeming liquidity tokens to return more tokens and less XTZ. It also causes the XTZ to token exchange rate to increase.

## Delegation

XTZ held by Dexter contracts can be delegated to bakers. The choice of baker is controlled by the manager contract.

## Included files

### Contracts
- [Dexter 2](./dexter.mligo) [(Compiled)](./dexter.mligo.tz)
- [Dexter 2 for FA2 tokens](./dexter.fa2.mligo) [(Compiled)](./dexter.fa2.mligo.tz)
- [FA1.2 implementation + custom mintOrBurn for LQT token](./lqt_fa12.mligo) [(Compiled)](./lqt_fa12.mligo.tz)



Contracts were compiled using Ligo built from source at revision
4d10d07ca05abe0f8a5fb97d15267bf5d339d9f4. This version does automatic
uncurrying, but does _not_ yet use Edo-only instructions, which are
not yet supported in Mi-Cho-Coq.

### Documentation

See this [overview](./docs/README.md).


### Other
- [Example origination script](./origination.sh)
- [dexter.mligo tests in LIGO](./dexter.test.mligo)

## Audits and Formal verification (External resources)
- [Runtime Verification](https://runtimeverification.com)'s formal verification using K-Michelson: [report](https://raw.githubusercontent.com/runtimeverification/publications/main/reports/smart-contracts/Tezos-Dexter.pdf); see also this [blog post](https://runtimeverification.com/blog/dexter-2-s-formal-verification).
- [Nomadic Labs](https://www.nomadic-labs.com/)' formal verification in Coq: [specification](https://gitlab.com/nomadic-labs/mi-cho-coq/-/blob/dexter_fa12lqt-verification/src/contracts_coq/dexter_spec.v) and [main proof file](https://gitlab.com/nomadic-labs/mi-cho-coq/-/blob/dexter_fa12lqt-verification/src/contracts_coq/dexter_verification_main.v).
