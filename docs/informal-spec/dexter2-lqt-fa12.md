# Dexter 2 Exchange: Informal specification of the liquidity token contract

This document is an informal specification for the liquidity token
contract of Dexter 2 (contract hash
`exprufAK15C2FCbxGLCEVXFe26p3eQdYuwZRk1morJUwy9NBUmEZVB`). See
[`dexter2`][dexter2] for an overview of Dexter 2.  See
[`dexter2-terminology`][dexter2-terminology] for a review of the
terminology used throughout this specification.

The liquidity token contract implements a non-fungible token, in
accordance with the FA1.2 standard.

The contract has two types of users: token holders, as introduced by
the FA1.2 standard (**liquidity providers**, from Dexter 2
perspective), and an admin (absent from the FA1.2 standard). The admin
is set during origination and cannot be changed. Token holders can
transfer their tokens (using the `%transfer` entrypoint) and approve
an allowance for another user (a **spender**) to transfer a
specificied amount of the tokens they hold (using the `%approve`
entrypoint).

The three [TZIP-4][tzip-4-views] view entrypoints `%getAllowance`,
`%getBalance` and `%getTotalSupply` allows any user to query the
contract's storage.

The fungible token implemented by the liquidity contract is **LQT**.
In addition to the standard FA1.2 entrypoints, the liquidity token
contract also implements a `%mintOrBurn` entrypoint that allows its
admin (the CPMM contract) to mint and burn tokens in a token holder's
account.

In the rest of the document, we prefer the gerenal-purpose FA1.2
terminology over the specific Dexter 2 terminology.

## Storage

 | Name           | Type                                 | Mutable | Comment                                 |
 |:---------------|:-------------------------------------|:--------|:----------------------------------------|
 | `tokens`       | `big_map address nat`                | Yes     | Maps token holder's to their balance    |
 | `allowances`   | `big_map (pair address address) nat` | Yes     | Maps (token holder, spender) to allowance |
 | `admin`        | `address`                            | No      | Address of the admin                    |
 | `total_supply` | `nat`                                | Yes     | Total supply of tokens                  |

## Errors

If an error condition is triggered (through the Michelson
[`FAILWITH`][mref-FAILWITH] instruction), then a `string` containing
an error code is given.

| Code                                            | Comment                                                                        |
|:------------------------------------------------|:-------------------------------------------------------------------------------|
| `"NotEnoughAllowance"`                          | Triggered if a spender initiates a transfer exceeding their allowance          |
| `"NotEnoughBalance"`                            | Triggered if a user initiates a transfer exceeding their balance               |
| `"UnsafeAllowanceChange"`                       | Triggered by unsafe allowances changes (see [`%approve`](#approve-entrypoint)) |
| `"Cannot burn more than the target's balance."` | Triggered if the admin attempts to burn more than a token holder's balance     |
| `"DontSendTez"`                                 | Triggered by any call to the contract that has tez attached                    |
| `"OnlyAdmin"`                                   | Triggered if a non-admin user calls `%mintOrBurn`                              |

In the informal specification of each entrypoint, we list the
conditions that will trigger an error if true (in the subsection
"Errors").

## Implicit parameters

For all entrypoint calls, we refer to the following implicit
parameters:

* **sender**: the sender responsible for calling the entrypoint, as
  returned by the Michelson [`SENDER`][mref-SENDER] instruction.
* **amount**: the amount of **XTZ**, in mutez, supplied in the transaction
  that initiated the current contract execution, as returned by the
  Michelson [`AMOUNT`][mref-AMOUNT] instruction.

## Entrypoints

The liquidity token contract has the following entrypoints.

 * [`%transfer`](#transfer-entrypoint): transfer tokens between two token holders
 * [`%approve`](#approve-entrypoint): approve a spender to transfer a token holder's tokens
 * [`%mintOrBurn`](#mintorburn-entrypoint): mint, or burn, tokens in a token holder's account
 * [`%getAllowance`](#getallowance-entrypoint): a TZIP-4 view for getting the allowance a token holder has given to a spender
 * [`%getBalance`](#getbalance-entrypoint): a TZIP-4 view for getting a token holder's balance
 * [`%getTotalSupply`](#gettotalsupply-entrypoint): a TZIP-4 view for getting the total supply of tokens


### `%transfer` entrypoint

Transfer `value` tokens from the token holder `from` to the
token holder `to`.

#### Parameters

```michelson
(pair %transfer (address %from) (pair (address %to) (nat %value)))
```

 | Name    | Type      |                              |
 |:--------|:----------|:-----------------------------|
 | `from`  | `address` | Account to debit             |
 | `to`    | `address` | Account to credit            |
 | `value` | `nat`     | Amount of tokens to transfer |


#### Logic

##### If **amount** is greater than zero:

We fail with `"DontSendTez"`.

##### If **sender** does not equal `from`:

In this case, we first verify that `from` has provided an allowance to
**sender**, and if so, update it.

Let `authorized_value` be the current allowance of `from` to
**sender**, i.e.  `allowances[from, sender]`, or 0 if there is no such
binding in `allowances`.

If `authorized_value < value`, fail with `"NotEnoughAllowance"`.

If `authorized_value == value`, then remove the binding of `from, sender` in `allowances`:

    allowances[from, sender] := undefined

Otherwise (if `authorized_value > value`), set the value of `from,sender`
to `authorized_value - value` in `allowances`:

    allowances[from, sender] := authorized_value - value

Let `from_balance` be the amount of tokens in the account of `from`, or 0 if
`from` has no binding in `tokens`.

If `from_balance < value`, fail with `"NotEnoughBalance"`.

If `from_balance == value`, then remove the binding of `from` in `tokens`:

    tokens[from] := undefined

Otherwise (if `from_balance > value`), set the value of `from` to
`from_balance - value` in `tokens`:

    tokens[from] := from_balance - value

Let `to_balance` be the amount of tokens in the account of `to`, or 0 if
`to` has no binding in `tokens`.

Let `new_balance` be `to_balance + value`. If `new_balance` is 0, then
remove the binding of `to` in `tokens`:

    tokens[to] = undefined

otherwise, update `to`'s balance:

    tokens[to] = new_balance

##### If **sender** equals `from`:

Let `from_balance` be the amount of tokens in the account of `from`, or 0 if
`from` has no binding in `tokens`.

If `from_balance < value`, fail with `"NotEnoughBalance"`.

If `from_balance == value`, then remove the binding of `from` in `tokens`:

    tokens[from] := undefined

Otherwise (if `from_balance > value`), set the value of `from` to
`from_balance - value` in `tokens`:

    tokens[from] := from_balance - value

Let `to_balance` be the amount of tokens in the account of `to`, or 0 if
`to` has no binding in `tokens`.

Let `new_balance` be `to_balance + value`. If `new_balance` is 0, then
remove the binding of `to` in `tokens`:

    tokens[to] = undefined

otherwise, update `to`'s balance:

    tokens[to] = new_balance

#### Operations

None.

#### Errors

| Error condition                                     | Error value            |
|:----------------------------------------------------|:-----------------------|
| `tokens[from] < value`                              | `"NotEnoughBalance"`   |
| `sender != from && allowances[from,sender] < value` | `"NotEnoughAllowance"` |

### `%approve` entrypoint

Approve the address `spender` to transfer up to `value` of the token holder **sender**'s tokens.

#### Parameters

```michelson
(pair %approve (address %spender) (nat %value))
```

 | Name      | Type      |                                                            |
 |:----------|:----------|:-----------------------------------------------------------|
 | `spender` | `address` | Address that will be allowed to spend **sender**'s tokens    |
 | `value`   | `nat`     | Token amount spender should be allowed to spend for sender |

#### Logic

If **amount** is greater than zero, fail with `"DontSendTez"`.

Let `current_allowance` be the current allowance,
i.e. `allowances[sender, spender]`, or 0 if there is no such binding
in `allowances`.

If `current_allowance` is not zero and value is not zero, fail
with `"UnsafeAllowanceChange"` [^1].

If `value` is 0, then remove any allowance from **sender** to `spender`
in `allowances`:

    allowances[sender, spender] = undefined

If `value` is not 0, then add update the allowance from **sender** to `spender`
in `allowances`:

    allowances[sender, spender] = value

#### Operations

None.

#### Errors


| Error condition                                 | Error value               |
|:------------------------------------------------|:--------------------------|
| `allowances[sender,spender] != 0 && value != 0` | `"UnsafeAllowanceChange"` |

### `%mintOrBurn` entrypoint

Mint (respectively burn) `quantity` tokens. Minted (respectively burned) tokens
are credited (respectively debited) from a given token holder `target`'s
account if `value` is positive (respectively negative).

#### Parameters

```michelson
(pair %mintOrBurn (int %quantity) (address %target))
```

 | Name       | Type      |                                                                                                |
 |:-----------|:----------|:-----------------------------------------------------------------------------------------------|
 | `quantity` | `int`     | Number of tokens that will be minted (if `quantity >= 0`), or burned (if `quantity < 0`)       |
 | `target`   | `address` | Token holder account in which tokens will be credited (when minting) or debited (when burning) |

#### Logic

If **amount** is greater than zero, fail with `"DontSendTez"`.

If **sender** does not equal `admin`, fail with `"OnlyAdmin"`.

Let `target_balance` be the amount of tokens in the account of `target`,
or 0 if `target` has no binding in `tokens`.

If quantity is negative and `target_balance < |quantity|`, fail with
`"Cannot burn more than the target's balance."`.

Otherwise, update storage as follows:

    tokens[target] := target_balance + quantity
    totalSupply := totalSupply + quantity

#### Operations

None.

#### Errors

| Error condition   | Error value   |
|:------------------|:--------------|
| `sender != admin` | `"OnlyAdmin"` |

### `%getAllowance` entrypoint

A [TZIP-4 view][tzip-4-views], passing the allowance a token holder
`owner` has given to a spender `spender` to the contract `callback`.

#### Parameters

```michelson
(pair %getAllowance (pair %request (address %owner) (address %spender)) (contract %callback nat))
```

 | Name       | Type           |                                   |
 |:-----------|:---------------|:----------------------------------|
 | `owner`    | `address`      | Address of the token holder       |
 | `spender`  | `address`      | Address of the spender            |
 | `callback` | `contract nat` | Callback to where send the result |

#### Logic

If **amount** is greater than zero, fail with `"DontSendTez"`.

Let `allowance` be the allowance given from `owner` to `spender`,
i.e. `allowances[owner, spender]`, or 0 if there is no such binding in
`allowances`.

Emit an internal transaction to `callback`, with the argument
`allowance` and no **XTZ** attached.

#### Operations

1. Transaction to `callback` with argument `allowance` and no **XTZ**
   attached.

#### Errors

None.

### `%getBalance` entrypoint

A [TZIP-4 view][tzip-4-views], passing the balance of a token holder
`owner` to the contract `callback`.

#### Parameters

```michelson
(pair %getBalance (address %owner) (contract %callback nat))
```


 | Name       | Type           |                                   |
 |:-----------|:---------------|:----------------------------------|
 | `owner`    | `address`      | Address of the token holder       |
 | `callback` | `contract nat` | Callback to where send the result |


#### Logic

If **amount** is greater than zero, fail with `"DontSendTez"`.

Let `balance` be the balance of `owner`, i.e. `tokens[owner]`, or 0 if
there is no such binding in `tokens`.

Emit an internal transaction to `callback`, with the argument
`balance` and no **XTZ** attached.

#### Operations

1. Transaction to `callback` with argument `balance` and no **XTZ**
   attached.

#### Errors

None.

### `%getTotalSupply` entrypoint

A [TZIP-4 view][tzip-4-views], passing the total token supply to
the contract `callback`.

#### Parameters

```michelson
(pair %getTotalSupply (unit %request) (contract %callback nat))
```


 | Name       | Type           |                                   |
 |:-----------|:---------------|:----------------------------------|
 | `request`  | `unit`         | A dummy value of type `unit`      |
 | `callback` | `contract nat` | Callback to where send the result |

#### Logic

If **amount** is greater than zero, fail with `"DontSendTez"`.

Emit an internal transaction to `callback`, with the argument
`totalSupply` and no **XTZ** attached.

#### Operations

1. Transaction to `callback` with argument `totalSupply` and no **XTZ**
   attached.

#### Errors

None.

[fa12]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md "TZIP-7: The FA1.2 standard"
[fa2]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md "TZIP-12: The FA2 standard"
[tzip-4-views]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-4/tzip-4.md#view-entrypoints "TZIP-4: Michelson Contract Interfaces and Conventions, section View entrypoints"

[mref-AMOUNT]: https://tezos.gitlab.io/michelson-reference/#instr-AMOUNT "The Michelson AMOUNT instruction"
[mref-BALANCE]: https://tezos.gitlab.io/michelson-reference/#instr-BALANCE "The Michelson BALANCE instruction"
[mref-FAILWITH]: https://tezos.gitlab.io/michelson-reference/#instr-FAILWITH "The Michelson FAILWITH instruction"
[mref-SENDER]: https://tezos.gitlab.io/michelson-reference/#instr-SENDER "The Michelson SENDER instruction"
[mref-SOURCE]: https://tezos.gitlab.io/michelson-reference/#instr-SOURCE "The Michelson SOURCE instruction"
[mref-NOW]: https://tezos.gitlab.io/michelson-reference/#instr-NOW "The Michelson NOW instruction"

[^1]: [ERC20 API: An Attack Vector on Approve/TransferFrom Methods](https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/)


[dexter2]: dexter2.md
[dexter2-cpmm]: dexter2-cpmm.md
[dexter2-terminology]: dexter2-terminology.md
[dexter2-lqt-fa12]: dexter2-lqt-fa12.md
