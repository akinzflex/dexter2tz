#!/usr/bin/env bash

set -eo pipefail

mkdir /tmp/dexter2_origination_example
# assumes tezos-client is on the PATH
mockup-client() {
    tezos-client --mode mockup --protocol PtEdo2Zk --base-dir /tmp/dexter2_origination_example "$@"
}

mockup-client create mockup

LQT_PROVIDER=$(mockup-client list known contracts | grep '^bootstrap1:' | cut -d' ' -f2)
MANAGER=$(mockup-client list known contracts | grep '^bootstrap2:' | cut -d' ' -f2)
# For simplicity, I assume that the initial tokenPool, xtzPool, and
# lqtTotal are all equal. We could generalize. IIRC, murbard claims
# that setting lqtTotal to sqrt(xtzPool * tokenPool) should be
# reasonable.
INITIAL_POOL=1000000

# Using lqt_fa12.mligo as an example token contract. Normally it will already exist.
# LQT_PROVIDER needs some tokens in this contract in order to provide liquidity
mockup-client originate contract token transferring 0 from bootstrap1 running ./lqt_fa12.mligo.tz \
              --init "Pair {Elt \"${LQT_PROVIDER}\" ${INITIAL_POOL}} {} \"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\" ${INITIAL_POOL}" \
              --burn-cap 10
TOKEN=$(mockup-client list known contracts | grep '^token:' | cut -d' ' -f2)

# Now we originate the dexter contract. Storage generated with:
# ligo compile-storage dexter.mligo main \
#   "build_storage { lqtTotal = ${INITIAL_POOL}n; manager = (\"${MANAGER}\" : address); tokenAddress = (\"${TOKEN}\" : address) }"
mockup-client originate contract dexter transferring 0 from bootstrap1 running ./dexter.mligo.tz \
              --init "Pair 0 0 ${INITIAL_POOL} False False \"${MANAGER}\" \"${TOKEN}\" \"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\"" \
              --burn-cap 10
DEXTER=$(mockup-client list known contracts | grep '^dexter:' | cut -d' ' -f2)

# Now we originate the LQT contract, giving an initial LQT balance to the
# LQT provider, and setting dexter as "admin"
mockup-client originate contract lqt transferring 0 from bootstrap1 running ./lqt_fa12.mligo.tz \
              --init "Pair {Elt \"${LQT_PROVIDER}\" ${INITIAL_POOL}} {} \"${DEXTER}\" ${INITIAL_POOL}" \
              --burn-cap 10
LQT=$(mockup-client list known contracts | grep '^lqt:' | cut -d' ' -f2)

# Now the LQT provider will actually provide the liquidity. We do this
# atomically so that the pool amounts become nonzero simultaneously.
mockup-client multiple transfers from ${LQT_PROVIDER} using "$(cat <<EOF
[
  {
    "destination": "${DEXTER}",
    "amount": "$(echo "${INITIAL_POOL} / 1000000" | bc)"
  },
  {
    "destination": "${TOKEN}",
    "entrypoint": "transfer",
    "amount": "0",
    "arg": "Pair \"${LQT_PROVIDER}\" \"${DEXTER}\" ${INITIAL_POOL}"
  },
  {
    "destination": "${DEXTER}",
    "entrypoint": "updateTokenPool",
    "amount": "0"
  }
]
EOF
)" --burn-cap 10

# Now the manager will set the lqtAddress
mockup-client transfer 0 from ${MANAGER} to ${DEXTER} \
              --entrypoint setLqtAddress \
              --arg \"${LQT}\"


# Now, does it work? Who knows, why test when you can write Coq proofs instead? ;)
